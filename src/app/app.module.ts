import {BrowserModule} from '@angular/platform-browser';
import {ErrorHandler, NgModule} from '@angular/core';
import {IonicApp, IonicErrorHandler, IonicModule} from 'ionic-angular';

import {MyApp} from './app.component';
import {HomePage} from '../pages/home/home';
import {ListPage} from '../pages/list/list';
import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {QrCodeProvider} from '../providers/qr-code/qr-code';
import {HttpClientModule} from '@angular/common/http';
import {GenerateQrCodePage} from "../pages/generate-qr-code/generate-qr-code";
import { SocialSharing } from "@ionic-native/social-sharing";
import { HistoryProvider } from '../providers/history/history';
import {IonicStorageModule} from "@ionic/storage";
import {BarcodeScanner} from "@ionic-native/barcode-scanner";
import {Dialogs} from "@ionic-native/dialogs";
import {Camera} from "@ionic-native/camera";

@NgModule({
    declarations: [
        MyApp,
        HomePage,
        ListPage,
        GenerateQrCodePage
    ],
    imports: [
        IonicStorageModule.forRoot(),
        BrowserModule,
        HttpClientModule,
        IonicModule.forRoot(MyApp),
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp,
        HomePage,
        ListPage,
        GenerateQrCodePage
    ],
    providers: [
        StatusBar,
        BarcodeScanner,
        SplashScreen,
        SocialSharing,
        Dialogs,
        Camera,
        {provide: ErrorHandler, useClass: IonicErrorHandler},
        QrCodeProvider,
        HistoryProvider,
    HistoryProvider
    ]
})
export class AppModule {
}
