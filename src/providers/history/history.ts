import {Injectable} from '@angular/core';
import {QrCode} from "../../models/QrCode";
import {Storage} from '@ionic/storage';
/*
  Generated class for the HistoryProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
const HISTORY_STORAGE_KEY: string = 'history';

@Injectable()
export class HistoryProvider {


    qrCodes:  Array<QrCode> = new Array<QrCode>();


    constructor(private storage: Storage) {
    }

    async add(text: string): Promise<void> {
        await this.load();
        this.qrCodes.push(new QrCode(text));
        return this.storage.set(HISTORY_STORAGE_KEY, this.qrCodes)
    }

    async load() {
        if (this.qrCodes) {
            return;
        } else {
            this.qrCodes = await this.storage.get(HISTORY_STORAGE_KEY) || [];
        }
    }
}
