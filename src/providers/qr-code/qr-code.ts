import {Injectable} from '@angular/core';
import qrcode from 'qrcode'
import {Camera, CameraOptions} from "@ionic-native/camera";
import jsQR, {QRCode} from "jsqr";

/*
  Generated class for the QrCodeProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/


@Injectable()
export class QrCodeProvider {
    private IMAGE_NOT_LOAD_EXCEPTION = 'Error when try to load image like a QRcode';
    private image : HTMLImageElement = null;
    private imageBase64 = '';
    constructor(private camera: Camera) {
        console.log('Hello QrCodeProvider Provider');
    }

    generate(text: string) {
        return new Promise<string>((resolve, reject) => {
            qrcode.toDataURL(text, function (err, url) {
                if (err) {
                    reject(err);
                } else {
                    resolve(url);
                }
            })

        })

    }

    async read(): Promise<string> {

        await this.initPicture();

        return new Promise<string>((resolve, reject) => {
            const canvas = document.createElement('canvas');
            const context = canvas.getContext('2d');
            const picture : HTMLImageElement = new Image();
            picture.src = this.imageBase64;
            picture.onload = () => {
                canvas.width = picture.width;
                canvas.height = picture.height;
                context.drawImage(picture, 0, 0);
                const imageCanvas : ImageData = context.getImageData(0, 0, canvas.width, canvas.height);
                resolve(jsQR(imageCanvas.data, imageCanvas.width, imageCanvas.height).data)
            };
            picture.onerror = () => {
                reject(this.IMAGE_NOT_LOAD_EXCEPTION)
            }
        });


    }

    private async initPicture() {
        const options: CameraOptions = {
            quality: 100,
            sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
            mediaType: this.camera.MediaType.PICTURE,
            encodingType: this.camera.EncodingType.JPEG,
            destinationType: this.camera.DestinationType.DATA_URL
        };
        this.image = await this.camera.getPicture(options);
        if (this.image != null) {
            this.imageBase64 = 'data:image/jpeg;base64,' + this.image;
        }
    }
}