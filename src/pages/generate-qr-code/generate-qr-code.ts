import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {QrCodeProvider} from "../../providers/qr-code/qr-code";
import { SocialSharing } from '@ionic-native/social-sharing';
import {HistoryProvider} from "../../providers/history/history";
/**
 * Generated class for the GenerateQrCodePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-generate-qr-code',
    templateUrl: 'generate-qr-code.html',
})
export class GenerateQrCodePage {

    textCode : string = '';
    generated = false;
    qrCode : string = '';
    isDisabled = true;

    constructor(public navCtrl: NavController, public navParams: NavParams,public history: HistoryProvider, public qrCodeService: QrCodeProvider, private socialSharing: SocialSharing) {

    }

    generateQrCode() {
        this.qrCodeService.generate(this.textCode)
            .then((data) => {
                this.qrCode = data;
                this.generated = true
                this.history.add(this.textCode)
            }).catch()
    }


    shareCode(): void {
        this.socialSharing.shareWithOptions({
            message: 'Scan le QRcode "' + this.textCode + '"',
            files: [this.qrCode]
        })
    }
}
