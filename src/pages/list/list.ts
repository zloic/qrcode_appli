import {Component} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import {HistoryProvider} from "../../providers/history/history";
import {QrCode} from "../../models/QrCode";
import {catchError} from "rxjs/operators";

@Component({
    selector: 'page-list',
    templateUrl: 'list.html'
})
export class ListPage {
    selectedItem: any;
    icons: string[];
    qrCodes: Array<QrCode> = new Array<QrCode>();

    constructor(public navCtrl: NavController, public navParams: NavParams, private historyService: HistoryProvider) {

    }


    ngOnInit() {
        this.historyService.load().then(() => {
            this.qrCodes = this.historyService.qrCodes;
            if (!this.historyService.load().then((data: any) => {
                if (data) {
                    this.qrCodes = data;
                    console.log(data)
                }
            })) {
                catchError(err => 'Error when try to retrieve qrcodes')
            }
        });
    }
}
