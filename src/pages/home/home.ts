import {Component} from '@angular/core';
import {NavController} from 'ionic-angular';
import {BarcodeScanner} from "@ionic-native/barcode-scanner";
import {Dialogs} from "@ionic-native/dialogs";
import {QrCodeProvider} from "../../providers/qr-code/qr-code";
import {utils} from "../../utils";

@Component({
    selector: 'page-home',
    templateUrl: 'home.html'
})
export class HomePage {

    private dialogTitle : string = 'QRcode Scan';

    constructor(public navCtrl: NavController, private barcode: BarcodeScanner, private  dialogs: Dialogs, private qrService: QrCodeProvider) {
    }

    scanFromCamera() {
        this.barcode.scan().then(data => {
            if(data.text){
                this.dialogs.alert(data.text, this.dialogTitle)
                    .then(() => console.log(utils.DIALOG_DISMISS))
                    .catch(e => console.log(utils.ERROR_DISPLAYING_DIALOG, e));
            }
        }).catch(err => {
            console.log('Error when try to scan QRcode', err);
        });
    }

    scanPicture(){
        this.qrService.read().then((data) =>{
            if(data){
                this.dialogs.alert(data, this.dialogTitle)
                    .then(() => console.log(utils.DIALOG_DISMISS))
                    .catch(e => console.log(utils.ERROR_DISPLAYING_DIALOG, e));
            }
        })
    }
}
